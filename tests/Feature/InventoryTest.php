<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InventoryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReturnOnlyWithProductName()
    {
        $response = $this->get('api/inventories?product_name=mr');
        $response->assertStatus(201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReturnAllInventoriesDataAccordingToSearchAndSortVariablet()
    {
        $response = $this->get('api/inventories?product_name=mr&vendor_name=mis&price=476.54&sort=rate,asc');
        $response->assertStatus(201);
    }
}
